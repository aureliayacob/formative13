SELECT m.name, b.name, a.street, p.amount, v.name, prod.stock
FROM product prod
JOIN manufacturer m ON prod.manufactureId = m.manufactureId
JOIN brand b ON prod.brandId = b.brandId
JOIN address a ON m.addressId = a.addressId
JOIN price p ON prod.productId= p.productId
JOIN valuta v ON p.valutaId = v.valutaId
WHERE prod.stock =0; 
