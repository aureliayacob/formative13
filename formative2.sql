INSERT INTO `brand` (`brandId`, `name`) VALUES (1,"Indofood");
INSERT INTO `brand` (`brandId`, `name`) VALUES (2,"Orang Tua");

INSERT INTO `address` (addressId, street, city, province, country, zipCode) 
VALUES (1, "Moorseelsesteenweg",  "Roeselare","Roeselare", "Belgium", "8800");
INSERT INTO `address` (addressId, street, city, province, country, zipCode) 
VALUES (2, "Arteri Kelapa Dua",  "Jakarta", "Jakarta Raya", "Indonesia", "11480");

INSERT INTO `manufacturer` (`manufactureid`, `name`, `addressId`)
VALUES (1, "Dewulf", 1);
INSERT INTO `manufacturer` (`manufactureid`, `name`, `addressId`)
VALUES (2, "Orang Tua Group", 2);


INSERT INTO `product` (`productId`, `artnumber`, `name`, `description`, `manufactureId`, `brandId`, `stock`) 
VALUES(1, 014935421, "Chitato BBQ", "Chitato potato barbeque falavored", 1, 1, 0);

INSERT INTO `product` (`productId`, `artnumber`, `name`, `description`, `manufactureId`, `brandId`, `stock`) 
VALUES(2, 014935421, "Chitato Sapi Panggang", "Chitato rasa sapi panggang", 1, 1, 0);

INSERT INTO `product` (`productId`, `artnumber`, `name`, `description`, `manufactureId`, `brandId`, `stock`) 
VALUES(3, 014935421, "Chitato Ayam Bumbu", "Chitato potato rasa ayam bumbu", 1, 1, 0);

INSERT INTO `product` (`productId`, `artnumber`, `name`, `description`, `manufactureId`, `brandId`, `stock`) 
VALUES(4, 014935421, "Chitato Origina", "Chitato potato rasa original", 1, 1, 0);


INSERT INTO `product` (`productId`, `artnumber`, `name`, `description`, `manufactureId`, `brandId`, `stock`) 
VALUES(5, 014935421, "Chitato Keju Supreme", "Chitato rasa keju super", 1, 1, 0);


INSERT INTO `product` (`productId`, `artnumber`, `name`, `description`, `manufactureId`, `brandId`, `stock`) 
VALUES(6, 017945425, "Tanggo Vanila", "Crispy wafers baked to an original recipe", 2, 2, 0);

INSERT INTO `product` (`productId`, `artnumber`, `name`, `description`, `manufactureId`, `brandId`, `stock`) 
VALUES(7, 011951426, "ChanchBox", "Thicker cream and crunchier waffle", 2, 2, 0);

INSERT INTO `product` (`productId`, `artnumber`, `name`, `description`, `manufactureId`, `brandId`, `stock`) 
VALUES(8, 011231424, "Walut", "A new fun way to enjoy wafer", 2, 2, 0);

INSERT INTO `product` (`productId`, `artnumber`, `name`, `description`, `manufactureId`, `brandId`, `stock`) 
VALUES(9, 011341425, "Fullo", "Get ready to fill up your days with fabulous Fullo", 2, 2, 0);

INSERT INTO `product` (`productId`, `artnumber`, `name`, `description`, `manufactureId`, `brandId`, `stock`) 
VALUES(10, 011121426, "Vita Pudding", "Enjoy a refreshing, creamy Vita Pudding", 2, 2, 0);

INSERT INTO  `valuta` (`valutaId`, `code`, `name` )
VALUES(1, "IDR", "rupiah");
INSERT INTO  `valuta` (`valutaId`, `code`, `name` )
VALUES(2, "USD", "US");

INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (1, 1, 1, 15000);
INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (2, 1, 2, 1);

INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (3, 2, 1, 30000);
INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (4, 2, 2, 2);

INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (5, 3, 1, 15000);
INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (6, 3, 2, 1);

INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (7, 4, 1, 15000);
INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (8, 4, 2, 1);

INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (9, 5, 1, 45000);
INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (10, 5, 2, 3);

INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (11, 6, 1, 15000);
INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (12, 6, 2, 1);

INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (13, 7, 1, 15000);
INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (14, 7, 2, 1);

INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (15, 8, 1, 45000);
INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (16, 8, 2, 3);

INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (17, 9, 1, 15000);
INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (18, 9, 2, 1);

INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (19, 8, 1, 30000);
INSERT INTO `price` (`priceId`, `productId`, `valutaId`, `amount`)
VALUES (20, 8, 2, 2);