CERATE TABLE`formativedb`.`address`(
`addressId` INT NOT NULL AUTO_INCREMENT,
`street` VARCHAR(45) ,
`city` VARCHAR(45) ,
`province` VARCHAR(45) ,
`country` VARCHAR(45) ,
`zipCode` VARCHAR(45) NOT NULL,
PRIMARY KEY(`addressId`));

CREATE TABLE `formativedb`.`manufacturer`(
`manufactureId` INT NOT NULL AUTO_INCREMENT,
`name` VARCHAR(45),
`addressId` INT NOT NULL,
PRIMARY KEY(`manufactureId`),
FOREIGN KEY (`addressId`) REFERENCES `address`(`addressId`));

CREATE TABLE `formativedb`.`brand`(
`brandId` INT NOT NULL AUTO_INCREMENT,
`name` VARCHAR(45),
PRIMARY KEY (`brandId`));

CREATE TABLE `formativedb`.`product`(
`productId` INT NOT NULL AUTO_INCREMENT,
`artNumber` INT NOT NULL,
`name` VARCHAR(45) NOT NULL,
`description` VARCHAR(50),
`manufactureId` INT,
`brandId` INT,
`stock` INT NOT NULL,
PRIMARY KEY(`productId`),
FOREIGN KEY(`manufactureId`) REFERENCES `manufacturer`(`manufactureId`),
FOREIGN KEY(`brandId`) REFERENCES `brand`(`brandId`));

CREATE TABLE `formativedb`.`price`(
`priceId` INT NOT NULL AUTO_INCREMENT,
`productId` INT,
`valutaId` INT,
`amount` INT NOT NULL,
PRIMARY KEY (`priceId`),
FOREIGN KEY (`valutaId`) REFERENCES `valuta`(`valutaId`));

CREATE TABLE `formativedb`.`valuta`(
`valutaId` INT NOT NULL AUTO_INCREMENT,
`code` VARCHAR(10) NOT NULL,
`name` VARCHAR(45) NOT NULL,
PRIMARY KEY(`valutaId`));
